/************************************
*									*
* Libreria para unopn de programas  *
*									*
*************************************/
/*************** Variables Globales ****************/
FILE *archivo;
int menor, mayor, rango = 0;
/******************** Fin *************************/


/***************** Prototipos *********************/

int getSize(char *);
void getData(int *, char *);
void getDataFloat(float *, char *);


void ordenarHojas(int *, int);
int minSequence(int *);
int potencia(int, int);
void imprimirDiagrama(int *, int, int);

float *ordenar(float *, int );
int cuartil1(int);
int mediana(int);
int cuartil3(int, int);
void diagrama(int, int, int, int, int, int);
void pinta(int , int , int , int );
void tpinta(int , int );

void generaHisto(int *,int *, int *, int , int);
void bubblesort(int *, int);
void llena(int *,int);
void calcularangos(int *);
void calcularango(int *);
void getMenor(int *, int);
void getMayor(int *, int);
void imprimirData(int *,int);
void imprimirHistograma(int *,int *,int);
/********************** Fin ***********************/


/*************** Funciones Generales ****************/
int getSize(char *argv){
	int number, length = 0;
	archivo = fopen(argv, "r");
	while(!feof(archivo)){
		fscanf(archivo, "%d", &number);
		length++;
	}
	fclose(archivo);
	return length;
}

void getData(int *datos, char *argv){
	int i = 0;
	archivo = fopen(argv, "r");
	while(!feof(archivo)){
		fscanf(archivo, "%d", &datos[i]);
		i++;
	}
	fclose(archivo);
}

void getDataFloat(float *datos, char *argv){
	int i = 0;
	archivo = fopen(argv, "r");
	while(!feof(archivo)){
		fscanf(archivo, "%f", &datos[i]);
		i++;
	}
	fclose(archivo);
}

/******************* Fin ************************/

/******************* Tallos y hojas *************/

//ordena las hojas de menor a mayor
void ordenarHojas(int a[], int total){
	int i, j, temp; 
	for (i=total; i>0;i--){
		for (j = 1; j <= i; j++){
			if (a[j-1] > a[j]){
				temp = a[j-1];
				a[j-1] = a[j];
				a[j] = temp;
			}
		}
	}
}

int minSequence(int a[]){
	int x=1, i=1;
	while(x>0){
		x=(a[0]-potencia(10, i));
		if(potencia(10, i)==10 && x<0)
			return 10;
		i++;
   }
   
	x=potencia(10, i-2);
	return x;
}

int potencia(int b, int x){
	int res=1, i;
	for (i=0; i<x; i++){
		res= res * b;
	}
	return res;
}

/* IMPRESION DEL DIAGRAMA DE TALLOS Y HOJAS*/
void imprimirDiagrama(int a[], int min, int total){
	int i=0, tallo=0;
	printf(" El diagrama de Tallos y Hojas es:\n\n");
	printf("\n");
	printf("\tTallos\t\tHojas\n");
	printf("\n");
	printf("\n\t%d\t|", tallo);
	while (i<total){
		if((a[i]-(tallo*min))<min){
			printf("%d  ", a[i]-(tallo*min));
			i++;
		
		}else{
			tallo++;
			printf("\n\t%d\t|", tallo);
		}
	}
	printf("\nTallos son\tHojas son las decenas\n");
	printf("las unidades\n\n");	
}

/******************* Fin ************************/

/******************* Diagrama de cajas *************/
float *ordenar(float *val, int tam){
	int i,j,tmp;
	for(i=1;i<tam;i++){
		j=i;
		tmp=val[i];
		while(j>0 && tmp<val[j-1]){
			val[j]=val[j-1];
			j--;
		}
		val[j]=tmp;
	}
	printf("\tDatos: ");
	for(i=0;i<tam;i++){
		printf("%d", (int)val[i]);
		printf(" ");
	}
	printf("\n\n");
	return val;
}

int cuartil1(int me){
	return (me) / 2;
}

int mediana(int tam){
	return (tam) / 2;
}

int cuartil3(int i, int me){
	return ((i-me)/2)+me;
}

void diagrama(int s1, int s2, int i,int q1,int q2,int q3){
	int tam, ran;
	tam = s2-s1;
	ran = tam / 2;
	pinta(s1,q1,1,tam);
	pinta(q1,q2,2,tam);
	pinta(q2,q3,3,tam);
	pinta(q3,s2,4,tam);
}

void pinta(int inicio,int fin, int tipo, int i){
	switch(tipo) {
		case 1:
			printf("\ts1->%d\t\t-\n",inicio);
			tpinta(((fin-inicio)*100)/i,1);
		break;
		
		case 2:
			printf("\tq1->%d\t--------|--------\n",inicio);
			tpinta(((fin-inicio)*100)/i,2);
		break;
		
		case 3:
			printf("\tMe->%d\t--------|--------\n",inicio);
			tpinta(((fin-inicio)*100)/i,2);
		break;
		
		case 4:
			printf("\tq3->%d\t--------|--------\n",inicio);
			tpinta(((fin-inicio)*100)/i,1);
			printf("\ts2->%d\t\t-\n",fin);
		break;
	}
}

void tpinta(int tam, int tipo){
	int i;
	for(i=0; i<(tam/5);i++){
		if (tipo == 2) {	
			printf("\t\t|   |   |\n");
		}else{
			printf("\t\t\t|\n");
		}
	}
}

/******************* Fin ************************/

/********************** Histograma **********************/
void generaHisto(int *datos,int *rangos, int *procesados, int size, int ran){
    for (int i = 0; i < size; i++){
        for(int j = 0; j < ran; j++){
            if(datos[i] >= rangos[j] && datos[i] < rangos[j+1])
                procesados[j]++;
        }
    }
}
void bubblesort(int *datos, int length){
     int i, j;
     float aux;
     for(i = 1; i < length; i++){
           for(j = 1; j < length - i; j++){
                 if(datos[j] > datos[j+1]){
                 aux = datos[j];
                 datos[j] = datos[j+1];
                 datos[j+1] = aux;
                 }
           }
     }
}
void llena(int *procesados,int rango){
    for (int i = 0; i < rango; i++){
        procesados[i] = 0;
    }
}
void calcularangos(int *rangos){
    int i;
    rangos[0] = menor;
    for (i = 1; i < rango; i++){
        rangos[i] = rangos[i-1] + 5;
    }
    rangos[i] = rangos[i-1] + 5;
}
void calcularango(int *rangos){
    int i;
    rangos[0] = menor;
    for (i = 1; i < mayor; i++){
        rangos[i] = rangos[i-1] + 1;
    }
    rangos[i] = rangos[i-1] + 1;
}
void getMenor(int *datos, int size){
    for (int i = 0; i < size; i++){
        if (i == 0){
            menor = datos[i];
        }
        if (datos[i] < menor){
            menor = datos[i];
        }
    }
}
void getMayor(int *datos, int size){
    for (int i = 0; i < size; i++){
        if (i == 0){
            mayor = datos[i];
        }
        if (datos[i] > mayor){
            mayor = datos[i];
        }
    }
}
void imprimirData(int *data,int size){
    printf("Datos:\n");
    for (int i = 0; i < size; i++){
        printf("%d,\t", data[i]);
    }
    printf("\n");
}
void imprimirHistograma(int *numeros,int *rangos,int size){
    printf( "%s%10s%14s \n", "Elemento", "Rango", "Histograma" );
    for (int i = 0; i < size; i++ ) {
        printf( "%7d%5s%d,%d%s \t", i,"[",rangos[i],rangos[i+1],")");
 
        for (int j = 1; j <= numeros[ i ]; j++ ) {   /* imprime una barra */
            printf( "%c", '*' );
        } /* fin del for interno */
 
        printf("\n");  /* fin de una barra del histograma */
    }/* fin del for externo */
}
/*************************** FIN ************************/