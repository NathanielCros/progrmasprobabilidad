#include <stdio.h>
#include <stdlib.h>
#include <string.h>

	FILE *archivo;
	
	//PROTOTIPO DE FUNCIONES//
	int obtenerRegistros();
	void getArray(int *);
	void ordenarHojas(int *, int);
	int minSequence(int *);
	int potencia(int, int);
	void imprimirDiagrama(int *, int, int);
	
		//FUNCIONES//
	//Leer los datos del archivo de texto
	int obtenerRegistros(){
		int total=0, buffer=0;
		archivo=fopen("datos.txt", "r");
		if(archivo==NULL){
			printf("No existe el fichero!\n"); 
		}
		//Se recorren los datos dentro del archivo txt hasta llegar al final//
		while(fscanf(archivo, "%d", &buffer)!=EOF){
			total++;
		}
		fclose(archivo);
		return total;
	}
		
	void getArray(int a[]){
		int i=0, buffer;
		archivo=fopen("datos.txt", "r");
		//Almacenamos los datos en cada tallo//
		while(fscanf(archivo, "%d", &buffer)!=EOF){
			a[i]=buffer;
			//Imprimimos los datos obtenidos//
			printf("%d)Dato: %d\n",i+1, a[i]);
			i++;
		}
		fclose(archivo);
	}
	
	//ordena las hojas de menor a mayor
	void ordenarHojas(int a[], int total){
		int i, j, temp; 
		for (i=total; i>0;i--){
			for (j = 1; j <= i; j++){
				if (a[j-1] > a[j]){
					temp = a[j-1];
					a[j-1] = a[j];
					a[j] = temp;
				}
			}
		}
	}

	int minSequence(int a[]){
        int x=1, i=1;
        while(x>0){
            x=(a[0]-potencia(10, i));
            if(potencia(10, i)==10 && x<0){
                return 10;
            }
			i++;
			getch();
        }
		x=potencia(10, i-2);
		return x;
    }

	int potencia(int b, int x){
		int res=1, i;
		for (i=0; i<x; i++){
			res= res * b;
		}
		return res;
	}
	/* IMPRESION DEL DIAGRAMA DE TALLOS Y HOJAS*/
    void imprimirDiagrama(int a[], int min, int total){
         int i=0, tallo=0;
		 printf(" El diagrama de Tallos y Hojas es:\n\n");
		 printf("\n");
		 printf("\tTallos\t\tHojas\n");
		 printf("\n");
		 printf("\n\t%d\t|", tallo);
		 while (i<total){
			if((a[i]-(tallo*min))<min){
				printf("%d  ", a[i]-(tallo*min));
				i++;
			}
			else{
				tallo++;
				printf("\n\t%d\t|", tallo);
			}
			
		 }
		 printf("\nTallos son\tHojas son las decenas\n");
		 printf("las unidades");	
    }