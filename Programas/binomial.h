#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double bin(long k, long n, double p);
double poisson(double lambda, double e);

double bin(long k, long n, double p){
    if (0 == k){
        return pow(1.0 - p, n);
    }
    return (double)(n - k +1) / k * (p/(1.0 - p) )* bin(k - 1, n, p);
}
double poisson(double x, double lambda){
	double poison = 0.0;
	double e = 2.7183;
	if(x == 0){
		poison = pow(e, -lambda);
	}else{
		poison = (double)(lambda/x) * ((double)p(x-1, lambda));
	}
	return poison;
}

