#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double combinacion(int n, int k);
double getProbBin(int n, int r, double p);

double combinacion(int n, int k){
    double res = 1.0, i = 1.0;
    
    if(k > (n - k)){ 
        while(i <= (n - k)){
            res = res*((k + i)/(i));  
            i++;  
        }
    }else{ 
        while(i <= k){
            res = res*((n - k + i)/(i));  
            i++;
        }
    }
    return(res);
}
double getProbBin(int n, int k, double p){
    double res = combinacion(n, k)*pow(p, k)*pow(1.0 - p , n - k);
    return(res);
}


double bin(long k, long n, double p){
    if (0 == k){
        return pow(1.0, n);
    }
    return ((double)(n - k +1)/k * ((double)p/(1.0 - p))*bin(k -1, n, p)
}
