#include <stdio.h>
#include <stdlib.h>
#include "histograma.h"

int main(int argc,char **argv){
    int size = 0, *datos, *procesados, *rangos, i;
    FILE *archivo = NULL;
    if(argc < 2){
        printf("Ingresar: Ejecutable <nombreEntrada>\n");
        exit(0);
    }
    if ((archivo = fopen(argv[1], "r")) == NULL){
        printf("No se pudo abrir el archivo\n");
        exit(0);
    }
    size = getSize(argv[1]);
    datos = (int*)malloc(sizeof(int) * size);
    getData(datos, argv[1]);
    getMenor(datos, size);
    getMayor(datos, size);

    bubblesort(datos, size);

    if(mayor > 10){
        for (i = menor; i < mayor; i+= 5, rango++);
        
        procesados = (int*)malloc(sizeof(int) * rango);
        rangos = (int*)malloc(sizeof(int) * (rango + 1));
        llena(procesados,rango);
        calcularangos(rangos);

        generaHisto( datos, rangos, procesados, size, rango);
        imprimirData(datos, size);

        imprimirHistograma(procesados,rangos,rango);
    }else{
        procesados = (int*)malloc(sizeof(int) * mayor);
        rangos = (int*)malloc(sizeof(int) * (mayor + 1));
        llena(procesados,mayor);
        calcularango(rangos);

        generaHisto( datos, rangos, procesados, size, mayor);
        imprimirData(datos, size);

        imprimirHistograma(procesados,rangos,mayor);
    }
 
    return 0;
}