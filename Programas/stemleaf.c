#include <stdio.h>
#include <stdlib.h>
#include "steam.h"
main(){
    int size = 0;
    FILE *archivo;
    if((archivo = fopen("datos.txt", "r")) == NULL){
        printf("No se pudo abrir el archivo");            
    }
    else{
        size = getSize(archivo);   
        float datos[size];
        getData(archivo, datos);
        printTreeGrip(datos, size);
        fclose(archivo);
    }
    system("pause");      
}
