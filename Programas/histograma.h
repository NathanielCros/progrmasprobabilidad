int menor, mayor, rango = 0;


void generaHisto(int *datos,int *rangos, int *procesados, int size, int ran){
    for (int i = 0; i < size; i++){
        for(int j = 0; j < ran; j++){
            if(datos[i] >= rangos[j] && datos[i] < rangos[j+1])
                procesados[j]++;
        }
    }
}
void bubblesort(int *datos, int length){
     int i, j;
     float aux;
     for(i = 1; i < length; i++){
           for(j = 1; j < length - i; j++){
                 if(datos[j] > datos[j+1]){
                 aux = datos[j];
                 datos[j] = datos[j+1];
                 datos[j+1] = aux;
                 }
           }
     }
}
void llena(int *procesados,int rango){
    for (int i = 0; i < rango; i++){
        procesados[i] = 0;
    }
}
void calcularangos(int *rangos){
    int i;
    rangos[0] = menor;
    for (i = 1; i < rango; i++){
        rangos[i] = rangos[i-1] + 5;
    }
    rangos[i] = rangos[i-1] + 5;
}
void calcularango(int *rangos){
    int i;
    rangos[0] = menor;
    for (i = 1; i < mayor; i++){
        rangos[i] = rangos[i-1] + 1;
    }
    rangos[i] = rangos[i-1] + 1;
}
void getData(FILE * file, int *datos, char *argv){
    int i = 0;
    file = fopen(argv, "r");
    while(!feof(file)){
        fscanf(file, "%d", &datos[i]);
        i++;
    }
    fclose(file);
}
int getSize(FILE *file){
    int number, length = 0;
    while(!feof(file)){
        fscanf(file, "%d", &number);
        length++;
    }
    return length;
}
void getMenor(int *datos, int size){
    for (int i = 0; i < size; i++){
        if (i == 0){
            menor = datos[i];
        }
        if (datos[i] < menor){
            menor = datos[i];
        }
    }
}
void getMayor(int *datos, int size){
    for (int i = 0; i < size; i++){
        if (i == 0){
            mayor = datos[i];
        }
        if (datos[i] > mayor){
            mayor = datos[i];
        }
    }
}
void imprimirData(int *data,int size){
    printf("Datos:\n");
    for (int i = 0; i < size; i++){
        printf("%d,\t", data[i]);
    }
    printf("\n");
}
void imprimirHistograma(int *numeros,int *rangos,int size){
    printf( "%s%10s%14s \n", "Elemento", "Rango", "Histograma" );
    for (int i = 0; i < size; i++ ) {
        printf( "%7d%5s%d,%d%s \t", i,"[",rangos[i],rangos[i+1],")");
 
        for (int j = 1; j <= numeros[ i ]; j++ ) {   /* imprime una barra */
            printf( "%c", '*' );
        } /* fin del for interno */
 
        printf("\n");  /* fin de una barra del histograma */
    }/* fin del for externo */
}