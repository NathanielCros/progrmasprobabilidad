#include "binomial.h"

int main()
{
    int n, k;
    double p;
    
    printf("Probabilidad binomial  P(# de Pruebas;  # de Exitos,  Probalidad Exito)\n");
    printf("\n# de Pruebas: ");
    scanf("%d", &n);
    printf("\n# de Exitos: ");
    scanf("%d", &k);
    
    if(n < k || n < 0 || k < 0){ 
        printf("# de pruebas < que el numero de exitos");
    }
    
    printf("\nProbabilidad de Exito: ");
    scanf("%lf", &p);
    
    if(p < 0.0 || p > 1.0){ 
        printf("\nLa probabilidad de exito no puede ser (p > 1) y (p < 0)\n");
    }
   
    p = getProbBin( n, k, p);
    
    printf("\n La probabilidad binomial es: %lf", p);

  system("pause");
}
