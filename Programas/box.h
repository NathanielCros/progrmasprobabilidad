int getSize(FILE *file);
void getData(FILE *file, float datos[]);
void printData(float datos[], int length);
void bubblesort(float datos[], int length);
float mean(float datos[], int length);
float median(float datos[], int length);
float variance(float datos[], int length);
float varianceAlt(float datos[], int length);
float standardDeviation(float varianza);
void printBox(float datos[], int length);
void divide(float datos[], float lower[], float upper[], int length);

void divide(float datos[], float lower[], float upper[], int length){
    int i, j;
    if(length%2 == 0){
        for(i = 1; i <= length/2; i++){
            lower[i] = datos[i];
        }
        j = 1;
        for(i = (length/2)+1; i <= length; i++){
            upper[j] = datos[i];
            j++;
        }
    }
    else{
        for(i = 1; i <= (length/2)+1; i++){
            lower[i] = datos[i];
        }
        j = 1;
        for(i = (length/2)+1; i <= length; i++){
            upper[j] = datos[i];
            j++;
        }
    }
}
void printBox(float datos[], int length){
    
    float lowerQuarter = 0.0, upperQuarter = 0.0;
    int i, j;
    bubblesort(datos, length);
    if(length%2 == 0){
        float lower[length/2], upper[length/2];
        divide(datos, lower, upper, length);
        lowerQuarter = median(lower, length/2);
        upperQuarter = median(upper, length/2);
        printf("Minimo: %f\nMaximo: %f\nMediana: %f\nCuarto Inferior: %f\nCuarto Superior: %f\n\n\n\n\n",
                 datos[1], datos[length], median(datos, length), lowerQuarter, upperQuarter);
        for(j = 0; j < 2; j++){
            for(i = 1; i <= length; i++){
                if(j == 0){
                    if(i < length/2){
                        if((lowerQuarter >= i) && (lowerQuarter <= i+1)){
                            printf("[  ");
                        }
                        else{
                            printf("- ");
                        }
                    }
                    else if(i == (length/2)+1){
                        printf("|  ");
                    }
                    else if(i > (length/2)+1){
                        if((upperQuarter >= i) && (upperQuarter <= i+1)){
                            printf("]  ");
                        }
                        else{
                            printf("- ");
                        }
                    }    
                }                
                else if(j == 1){
                    printf("%d ", i);    
                }
            }printf("\n");               
        }
    }
    else{
        float lower2[(length/2)+1], upper2[(length/2)+1];
        divide(datos, lower2, upper2, length);
        lowerQuarter = median(lower2, (length/2)+1);
        upperQuarter = median(upper2, (length/2)+1);
        printf("Minimo: %f\nMaximo: %f\nMediana: %f\nCuarto Inferior: %f\nCuarto Superior: %f\n\n\n\n\n",
                 datos[1], datos[length], median(datos, length), lowerQuarter, upperQuarter);
        for(j = 0; j < 2; j++){
            for(i = 1; i <= length; i++){
                if(j == 0){
                    if(i < (length/2)+1){
                        if((lowerQuarter >= i) && (lowerQuarter <= i+1)){
                            printf("[  ");
                        }
                        else{
                            printf("- ");
                        }
                    }
                    else if(i == (length/2)+1){
                        printf("|  ");
                    }
                    else if(i > (length/2)+1){
                        if((upperQuarter >= i) && (upperQuarter <= i+1)){
                            printf("]  ");
                        }
                        else{
                            printf("- ");
                        }
                    }    
                }                
                else if(j == 1){
                    printf("%d ", i);    
                }
            }printf("\n");               
        }
    }
}
float standardDeviation(float varianza){
    return sqrt(varianza);    
}
float variance(float datos[], int length){
      int i;
      float media = mean(datos, length);
      float varianza = 0.0;
      for(i = 1; i <= length; i++){
          varianza = varianza + (datos[i] - media)*(datos[i] - media); 
      }
      varianza = varianza/length;
      return varianza;
}
float varianceAlt(float datos[], int length){
      int i;
      float var;
      float sum = 0.0;
      float sum2 = 0.0;
      float sample;
      for(i = 1; i <= length; i++){
          sum = sum + (datos[i]);
          sum2 = sum2 + (datos[i] * datos[i]); 
      }
      var = ((float)length * sum2 - (sum * sum))/ (float) (length * (length - 1));
      return var;
}
float median(float datos[], int length){
      float median;
      bubblesort(datos, length);
      if(length%2 == 0){
          median = (datos[length/2] + datos[(length/2)+1])/2;
      }
      else{
          median = datos[(length+1)/2];
      }
      return median;
}
void bubblesort(float datos[], int length){
     int i, j;
     float aux;
     for(i = 1; i <= length; i++){
           for(j = 1; j <= length - i; j++){
                 if(datos[j] > datos[j+1]){
                 aux = datos[j];
                 datos[j] = datos[j+1];
                 datos[j+1] = aux;
                 }
           }
     }
}
float mean(float datos[], int length){
      int i;
      float median = 0.0;
      for(i = 1; i <= length; i++){
            median = median + datos[i];
      }
      median = median / length;

      return median;
}

void printData(float datos[], int length){
     int i;
     printf("\n");
     for(i = 1;i <= length; i++){
           printf("%f\n", datos[i]);
     }
     printf("\n");
}
void getData(FILE * file, float datos[]){
    int i = 1;
    file = fopen("datos.txt", "r");
    while(!feof(file)){
        fscanf(file, "%f", &datos[i]);
        i++;
    }
    fclose(file);
}
int getSize(FILE *file){
    int number, length = 0;
    while(!feof(file)){
        fscanf(file, "%f", &number);
        length++;
    }
    return length;
}
