void printTreeGrip(float datos[], int size);
int getSize(FILE *file);
void getData(FILE *file, float datos[]);

void getData(FILE * file, float datos[]){
    int i = 0;
    file = fopen("datos.txt", "r");
    while(!feof(file)){
        fscanf(file, "%f", &datos[i]);
        i++;                  
    }
    fclose(file);
}
int getSize(FILE *file){
    int number, size = 0;
    while(!feof(file)){            
        fscanf(file, "%f", &number);
        size++;       
    }
    return size;
}
void printTreeGrip(float datos[], int size){
    int i = 0, j = 0, stem[size], entero = 0, leaf[size];
    float temporal = 0.0;
    for(i = 0; i < size; i++){
        temporal = datos[i]*10;
        entero = temporal;
        stem[i] = entero/10;
        leaf[i] = entero%10;  
    }
     printf("Tallos \t   Hojas\n");
        for(i = 1; i < 10; i++){
            printf("\n%d\t| ", i);
        for(j = 0; j < size; j++){
                if(i == stem[j]){
                printf(" %d", leaf[j]);
            }
        }
        printf("\n");               
    }
}
