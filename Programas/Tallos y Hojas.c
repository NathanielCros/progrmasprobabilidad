/*Diagrama de Tallos y Hojas*/
/*Probabilidad y Estadística*/
/*Viorel Urban Urban*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include "TyH.h"


	//	FUNCION MAIN//
	int main (){
		int *datos; //creamos un arreglo dinamico
		int i=0, tallos, hojas;
		tallos=obtenerRegistros();//guardamos el numnero total de datos leidos del archivo
		datos=(int*) malloc(tallos*sizeof(int)); //tipo del arreglo int y le asignamos un tamaño
		getArray(datos);
		ordenarHojas(datos, tallos);
		printf ("\n");
		hojas=minSequence(datos);
		imprimirDiagrama(datos, hojas, tallos);
		getch();
	}
	
