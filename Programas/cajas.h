#include <stdio.h>
#include <stdlib.h>


float *ordenar(float *val, int tam);
int cuartil1(int me);
int mediana(int tam);
int cuartil3(int i, int me);
void diagrama(int s1, int s2, int i,int q1,int q2,int q3);
void pinta(int s1, int q1, int tipo, int i);
void tpinta(int tam, int tipo);


float *ordenar(float *val, int tam){
    int i,j,tmp;
    for(i=1;i<tam;i++){
        j=i;
        tmp=val[i];
        while(j>0 && tmp<val[j-1]){
            val[j]=val[j-1];
            j--;
        }
        val[j]=tmp;
    }
    printf("\tDatos: ");
    for(i=0;i<tam;i++){
        printf("%d", (int)val[i]);
        printf(" ");
    }
    printf("\n\n");
    return val;
}
int cuartil1(int me){
    return (me) / 2;
}
int mediana(int tam){
    return (tam) / 2;
}
int cuartil3(int i, int me){
    return ((i-me)/2)+me;
}
void diagrama(int s1, int s2, int i,int q1,int q2,int q3){
    int tam, ran;
    tam = s2-s1;
    ran = tam / 2;
    pinta(s1,q1,1,tam);
    pinta(q1,q2,2,tam);
    pinta(q2,q3,3,tam);
    pinta(q3,s2,4,tam);
}
void pinta(int inicio,int fin, int tipo, int i){
    switch(tipo) {
        case 1:
            printf("\ts1->%d\t\t-\n",inicio);
            tpinta(((fin-inicio)*100)/i,1);
            break;
        case 2:
            printf("\tq1->%d\t--------|--------\n",inicio);
            tpinta(((fin-inicio)*100)/i,2);
            break;
        case 3:
            printf("\tMe->%d\t--------|--------\n",inicio);
            tpinta(((fin-inicio)*100)/i,2);
            break;
        case 4:
            printf("\tq3->%d\t--------|--------\n",inicio);
            tpinta(((fin-inicio)*100)/i,1);
            printf("\ts2->%d\t\t-\n",fin);
            break;
    }
}
void tpinta(int tam, int tipo){
    int i;
    for(i=0; i<(tam/5);i++){
        if (tipo == 2) {
            printf("\t\t|       |       |\n");
        }
        else{
            printf("\t\t\t|\n");
        }
    }
}
