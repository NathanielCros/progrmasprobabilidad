/************************************
Menu para programas de probabilidad

Integrantes de equipo y autores:

Frida
Lizbeth
Oscar Nathaniel
Viorel Urban

1.- Tallos y Hojas
2.- Diagrama de cajas
3.- Histograma
******************/   
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Menu.h"

int main(int argc,char **argv){
	int opc, size = 0, i = 0, *datos;
	if(argc < 2){
printf("Ingresar: Ejecutable <nombreEntrada>\n");
exit(0);
}
if ((archivo = fopen(argv[1], "r")) == NULL){
printf("No se pudo abrir el archivo\n");
exit(0);
}

	do{
		printf("1.TALLOS Y HOJAS.\n");
		printf("2.DIAGRAMA DE CAJA.\n");
		printf("3.HISTROGRAMA.\n");
		printf("4.SALIR.\n");
		printf("\nIntroduzca su opcion\n");

		fflush( stdin );
		scanf("%d", &opc);

		switch(opc){

			case 1://TALLOS Y HOJAS
				
				printf("DIAGRAMA DE TALLOS Y HOJAS\n");
				
				int tallos, hojas;

				tallos=getSize(argv[1]);//guardamos el numnero total de datos leidos del archivo
				datos=(int*) malloc(tallos*sizeof(int)); //tipo del arreglo int y le asignamos un tamaño
				
				getData(datos, argv[1]);
				ordenarHojas(datos, tallos);
				printf ("\n");
				
				hojas=minSequence(datos);
				imprimirDiagrama(datos, hojas, tallos);
			break;

			case 2://DIAGRAMA DE CAJA
				printf("DIAGRAMA DE CAJA.\n"); 
				
				float *num, *temp;
				int q1,q2,q3,s1,s2;
				size = getSize(argv[1]);

				num = (float*)malloc(size*sizeof(float));

				getDataFloat(num,argv[1]);

				num = ordenar(num,size);

				q2 = mediana(size);
				q1 = cuartil1(q2);
				q3 = cuartil3(size,q2);
				s1=0;
				s2=size;

				diagrama(num[s1],num[s2-1],s2,num[q1],num[q2],num[q3]);
			break;

			case 3://HISTOGRAMA
				printf("HISTOGRAMA\n");

				int *procesados, *rangos;

				size = getSize(argv[1]);
				datos = (int*)malloc(sizeof(int) * size);
				getData(datos, argv[1]);
				getMenor(datos, size);
				getMayor(datos, size);

				bubblesort(datos, size);

				if(mayor > 10){
					for (i = menor; i < mayor; i+= 5, rango++);
					
					procesados = (int*)malloc(sizeof(int) * rango);
					rangos = (int*)malloc(sizeof(int) * (rango + 1));
					llena(procesados,rango);
					calcularangos(rangos);

					generaHisto( datos, rangos, procesados, size, rango);
					imprimirData(datos, size);

					imprimirHistograma(procesados,rangos,rango);
				}else{
					procesados = (int*)malloc(sizeof(int) * mayor);
					rangos = (int*)malloc(sizeof(int) * (mayor + 1));
					llena(procesados,mayor);
					calcularango(rangos);

					generaHisto( datos, rangos, procesados, size, mayor);
					imprimirData(datos, size);

					imprimirHistograma(procesados,rangos,mayor);
				}
			break;

			case 4:
				printf("Gracias Por participar\n");
			break;

			default:
				printf("Esa opcion no existe.\n");
			break;

		}
	}while(opc != 4);
	return 0;
}